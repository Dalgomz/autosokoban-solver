import Sokoban
import sys
from random import randint
sys.path.append("SokoSolver.jar")
import javaTesterIJK

#Rutas de niveles =============================
# Niveles/nivel1.txt
# Niveles/nivel2.txt
# Niveles/nivel3.txt

# Fin rutas ===================================
def jugadorManual(arch):
    board = Sokoban.Board(arch)
    board.Print()
    
    gano = -1
    while (gano != 0 and gano != 2):
        mov = raw_input()
        # crear funcion random para obtener una letra entre w,a,s,d
        # la letra obtenida se pasa a la funcion movimientos.,
        board.movimientos(mov)
        print("====================================================================================")
        board.Print()
        gano = board.estadoJugador()
    print("GAME OVER")
    if gano == 0:
        print("YOU LOSE")
    else:
        if gano == 2:
            print("YOU WIN")

def jugadorAutomatico(arch):
    board = Sokoban.Board(arch)
    board.Print()
    solver = javaTesterIJK()
    movimientos = solver.resolver(arch)
    board.jugadorAutomatico(list(movimientos))



arch = "./Niveles/mapa_"
random = randint(1,17)
arch += str(random) + ".txt"
print ("==========MENU===========")
print ("1 -> Para jugador manual")
print ("2 -> Para jugador automatico")
selection = raw_input()
if(selection == "1"):
    jugadorManual(arch)
elif (selection == "2"):
    jugadorAutomatico(arch)
else:
    print("Dato Invalido")
    
