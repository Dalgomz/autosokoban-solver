import java.util.ArrayList;

public class Solver {
	//Se van a calcular al final
	private ArrayList<Character> movimientos;

	// Lista din�mica de estados
	private ArrayList<Tablero> estados;
	// Arbol de recuerdos
	private Tree recuerdos;


	public Solver(Tablero inicial) {
		this.recuerdos = new Tree(inicial);
		this.estados = this.recuerdos.resolver();
	}

	// TODO armar camino entre todos los estados y pegarlos all�
	public String getMovimientos() {
		String secuencia = "";
		for(int i=0;i<estados.size()-1;i++) {

			int dir = Tablero.direccionMovimiento(estados.get(i), estados.get(i+1));
			movimientos = estados.get(i).caminoEntreAB(
					estados.get(i).getPlayerX(), estados.get(i).getPlayerY(),
					estados.get(i+1).getPlayerX(), estados.get(i+1).getPlayerY(),dir);
				for(Character x :movimientos)
					if(x != 'x')
						secuencia = secuencia + x;

		}
		return secuencia;
	}
	
	public ArrayList<Tablero> getEstados() {
		return this.estados;
	}
	
	public void mostrarSolucion() {
		for(int i=0;i<estados.size();i++) {
			estados.get(i).showTab();
		}
		if(estados.isEmpty())
			System.out.println("Sin Soluci�n");
	}
}
