import java.util.ArrayList;

public class AStarNodo {
	private boolean[][] dato;
	public int posx;
	public int posy;
	private int mx;
	private int my;
	private ArrayList<AStarNodo> hijos;
	public int valorH;

	public AStarNodo(boolean x[][], int posx, int posy, int mx, int my) {
		this.dato = x;
		this.hijos = new ArrayList<>();
		this.posx = posx;
		this.posy = posy;
		this.mx = mx;
		this.my = my;
		this.valorH = valorHeuristico(posx, posy, mx, my);
	}

	public boolean[][] getDato() {
		return this.dato;
	}

	private int valorHeuristico(int i, int j, int gox, int goy) {
		return abs(i-gox) + abs(j-goy);
	}

	private int abs(int a) {
		if (a<0)
			return -a;
		else
			return a;
	}

	public String toString() {
		return dato.toString();
	}

	public ArrayList<Character> resolver(ArrayList<Character> solucion, AStarTree recuerdos) {
		
		if(this.posx == mx && this.posy == my) {
			solucion.add('x');
			return solucion;
		}

		for(int i=1;i<5;i++) {
			AStarNodo cand = mover(i);
			if(cand != null) {
				if(!recuerdos.search(cand.posx,cand.posy)) {
					hijos.add(cand);
				}
			}
		}

		this.hijos.sort(new AStarCompare());

		for(AStarNodo hijoActual : hijos) {
			solucion = hijoActual.resolver(solucion, recuerdos);
			if(!solucion.isEmpty()) {
				solucion.add(0, this.cambio(hijoActual));
				return solucion;
			}
		}

		return solucion;
	}

	private Character cambio(AStarNodo hijoActual) {
		int auxX = this.posx - hijoActual.posx;
		int auxY = this.posy - hijoActual.posy;
		if(auxX == -1) 
			return 'D';
		else if(auxX == 1)
			return 'A';
		else if(auxY == -1)
			return 'S';
		else
			return 'W';
	}

	private AStarNodo mover(int dir) {
		AStarNodo nuevo = null;
		switch(dir) {
		case(1):
			if( this.dato[posx-1][posy]) 
				nuevo = new AStarNodo(dato, posx-1, posy, mx, my);
		break;
		case(2):
			if(this.dato[posx][posy-1])
				nuevo = new AStarNodo(dato, posx, posy-1, mx, my);
		break;
		case(3):
			if(this.dato[posx+1][posy])
				nuevo = new AStarNodo(dato, posx+1, posy, mx, my);
		break;
		case(4):
			if(this.dato[posx][posy+1]) 
				nuevo = new AStarNodo(dato, posx, posy+1, mx, my);
		break;
		}
		return nuevo;
	}

	public boolean search(int posx, int posy) {
		if(this.posx == posx && this.posy == posy) 
			return true;
		else {
			for(int i=0;i<this.hijos.size();i++) {
				if(hijos.get(i).search(posx,posy))
					return true;
			}
			return false;
		}
	}
}
