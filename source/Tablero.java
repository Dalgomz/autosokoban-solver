import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

// Width i,  Height j
public class Tablero  {

	public static final char MURO = 'X';
	public static final char JUGADOR = '@';
	public static final char JUGADORM = '&';
	public static final char CAJA = '$';
	public static final char META = 'M';
	public static final char CAJAM = '*';
	public static final char LIBRE = '_';

	private int width;
	private int height;
	private char data[][];
	private int charX;
	private int charY;
	private int vHeuristico;

	public Tablero(int width, int height, char[][] data, int charX, int charY) {
		this.width = width;
		this.height = height;
		this.charX = charX;
		this.charY = charY;
		this.data = new char[width][height]; 

		for(int i=0;i<width;i++) {
			for(int j=0;j<height;j++) {
				this.data[i][j] = data[i][j];
			}
		}
	}

	public Tablero(String path) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(path));
			String line = reader.readLine();
			this.width = Integer.parseInt(line);
			line = reader.readLine();
			this.height = Integer.parseInt(line);
			int cont = height;
			String data = "";
			while (cont != 0) {
				line = reader.readLine();
				data = data + line;
				cont--;
			}
			reader.close();

			this.data = new char[width][height]; 

			for(int i=0;i<height;i++) {
				for(int j=0;j<width;j++) {
					this.data[j][i] = data.charAt(j + i*width);
					if(this.data[j][i] == JUGADOR || this.data[j][i] == JUGADORM) {
						this.charX = j;
						this.charY = i;
					}
				}
			}
			this.vHeuristico = 0;

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public char getPos(int x, int y) {
		char aux = data[x][y]; 
		return aux;
	}

	public final char[][] getData() {
		char[][] aux = new char[width][height];
		for(int i=0;i<width;i++) {
			for(int j=0;j<height;j++) {
				aux[i][j] = data[i][j];
			}
		}
		return aux;
	}

	private char[][] getTrueData(){
		return data;
	}

	public int getPlayerX() {
		return charX;
	}
	public void setPlayerX(int charX) {
		this.charX = charX;
	}
	public int getPlayerY() {
		return charY;
	}
	public void setPlayerY(int charY) {
		this.charY = charY;
	}
	public int getValor() {
		return vHeuristico;
	}
	public void setValor() {
		//TODO

		this.vHeuristico = 0;
		for(int i=0;i<width;i++) {
			for(int j=0;j<height;j++) {
				int xx,yy;
				xx=0;
				yy=0;
				if(data[i][j] == CAJA) {
					int n = 3, r = 1;
					boolean keep = true;
					while(2*r < width && keep) {
						for(int x=0;x<n-1 && keep;x++) {
							if(i-r < width && i-r >= 0 && j-r+x < height && j-r+x >= 0)
								if(data[i-r][j-r+x] == META) {
									keep = false;
									xx = i-r;
									yy = j-r+x;
								}
						}
						for(int x=0;x<n-1 && keep;x++) {
							if(i-r+x < width && j+r <height && i-r+x >=0 && j+r >= 0)
								if(data[i-r+x][j+r] == META) {
									keep = false;
									xx = i-r+x;
									yy = j+r;
								}
						}
						for(int x=0;x<n-1 && keep;x++) {
							if(i+r < width && j+r-x <height && i+x >=0 && j+r-x >= 0)
								if(data[i+r][j+r-x] == META) {
									keep = false;
									xx = i+r;
									yy = j+r-x;
								}
						}
						for(int x=0;x<n-1 && keep;x++) {
							if(i+r-x < width && j-r <height && i+r-x >=0 && j-r >= 0) 
								if(data[i+r-x][j-r] == META) {
									keep = false;
									xx = i+r-x;
									yy = j-r;
								}
						}
						n += 2;
						r++;
					}
					this.vHeuristico += abs(i-xx) + abs(j-yy);
				}
			}
		}
	}

	private int abs(int a) {
		if (a<0)
			return -a;
		else
			return a;
	}

	// 1 = Arriba | 2 = Izquierda | 3 = Abajo | 4 = Derecha
	public boolean posibleEmpujarCaja(int x, int y, int dir) {
		switch(dir) {
		case(1):{
			if(y != 0 && data[x][y-1] == LIBRE || data[x][y-1] == META || data[x][y-1] == JUGADORM || data[x][y-1] == JUGADOR) 
				return puedeLlegar(charX,charY,x,y+1);
			break;
		}
		case(2):{
			if(x != 0 && data[x-1][y] == LIBRE|| data[x-1][y] == META || data[x-1][y] == JUGADORM || data[x-1][y] == JUGADOR) 
				return puedeLlegar(charX,charY,x+1,y);
			break;
		}
		case(3):{
			if(y != height && data[x][y+1] == LIBRE || data[x][y+1] == META || data[x][y+1] == JUGADORM || data[x][y+1] == JUGADOR)
				return puedeLlegar(charX,charY,x,y-1);
			break;
		}
		case(4):{
			if(x != width && data[x+1][y] == LIBRE || data[x+1][y] == META || data[x+1][y] == JUGADORM || data[x+1][y] == JUGADOR)
				return puedeLlegar(charX,charY,x-1,y);
			break;
		}
		}
		return false;
	}

	public boolean puedeLlegar(int xa, int ya, int xb, int yb) {
		int y = width;
		int x = height;
		boolean [][] posibles = new boolean[y][x];
		for (int i = 0; i < y; i++){
			for (int j = 0; j < x; j++){
				posibles[i][j] = false;
			}
		}
		llenarMatriz(getData(), posibles, xa, ya);
		return posibles[xb][yb];
	}

	public ArrayList<Character> caminoEntreAB(int xa, int ya, int xb, int yb, int dir) {

		System.out.println(xa + " " + ya);
		System.out.println(xb + " " + yb);
		System.out.println(this.data[xa][ya]);
		System.out.println(this.data[xb][yb]);
		switch(dir) {
		case(1):{
			yb++;
			break;
		}
		case(2):{
			xb--;
			break;
		}
		case(3):{
			yb--;
			break;
		}
		case(4):{
			xb++;
			break;
		}
		}
		boolean M[][] = new boolean[width][height];
		llenarMatriz(getData(), M, xa, ya);
		AStarTree camino = new AStarTree(M,xa,ya,xb,yb);
		ArrayList<Character> aux;
		aux = camino.resolver();
		switch(dir) {
		case(1):{
			aux.add('W');
			break;
		}
		case(2):{
			aux.add('D');
			break;
		}
		case(3):{
			aux.add('S');
			break;
		}
		case(4):{
			aux.add('A');
			break;
		}
		}
		
		return aux;
	}

	public void llenarMatriz(char [][] tablero, boolean [][] M, int posy, int posx){
		int y = M.length;
		int x = M[0].length;
		if (tablero[posy][posx]!= MURO && tablero[posy][posx]!= CAJA && tablero[posy][posx]!= CAJAM){
			M[posy][posx] = true;
			if (posy > 0 && !M[posy-1][posx]){
				llenarMatriz(tablero, M, posy-1, posx);
			}
			if (posy < y-1){
				llenarMatriz(tablero, M, posy+1, posx);
			}
			if (posx > 0 && !M[posy][posx-1]){
				llenarMatriz(tablero, M, posy, posx-1);
			}
			if (posx < x-1){
				llenarMatriz(tablero, M, posy, posx+1);
			}
		}
	}

	public void imprimirMatriz(boolean [][] M){
		for (int i = 0; i < M.length; i++){
			for (int j = 0; j < M[0].length; j++){
				if(M[i][j])
					System.out.print("T "); 
				else
					System.out.print("F "); 
			}
			System.out.println(" ");
		}
	}

	public boolean deadlocked() {
		for(int i=0;i<width;i++) 
			for(int j=0;j<height;j++) 
				if(data[i][j] == CAJA) { 
					if(cajaEnEsquina(i,j)) 
						return true;
					else if(cajaPegada(i,j))
						return true;
					else if(cajas2ContraPared(i,j))
						return true;
					else if(cajasEnBloque(i,j))
						return true;
				}

		return false;
	}

	private boolean cajaEnEsquina(int i, int j) {
		if(data[i-1][j] == MURO && data[i][j-1] == MURO)
			return true;

		else if(data[i-1][j] == MURO && data[i][j+1] == MURO)
			return true;

		else if(data[i+1][j] == MURO && data[i][j-1] == MURO)
			return true;

		else if(data[i+1][j] == MURO && data[i][j+1] == MURO)
			return true;

		return false;
	}

	private boolean cajaPegada(int i, int j) {
		int auxUpp, auxBot;
		if(data[i][j-1] == MURO) {
			auxUpp = i;
			while(data[auxUpp][j] != MURO) 
				if(data[auxUpp][j] == META || data[auxUpp][j] == JUGADORM)
					return false;
				else
					auxUpp++;
			auxBot = i;
			while(data[auxBot][j] != MURO) 
				if(data[auxBot][j] == META || data[auxBot][j] == JUGADORM)
					return false;
				else
					auxBot--;

			for(int k=auxBot+1;k<auxUpp;k++) {
				if(data[k][j-1] != MURO)
					return false;
			}

			return true;
		}
		if(data[i][j+1] == MURO) {
			auxUpp = i;
			while(data[auxUpp][j] != MURO) 
				if(data[auxUpp][j] == META || data[auxUpp][j] == JUGADORM)
					return false;
				else
					auxUpp++;
			auxBot = i;
			while(data[auxBot][j] != MURO) 
				if(data[auxBot][j] == META || data[auxBot][j] == JUGADORM)
					return false;
				else
					auxBot--;

			for(int k=auxBot+1;k<auxUpp;k++) {
				if(data[k][j+1] != MURO)
					return false;
			}
			return true;
		}
		if(data[i-1][j] == MURO) {
			auxUpp = j;
			while(data[i][auxUpp] != MURO) 
				if(data[i][auxUpp] == META || data[i][auxUpp] == JUGADORM)
					return false;
				else
					auxUpp++;
			auxBot = j;
			while(data[i][auxBot] != MURO) 
				if(data[i][auxBot] == META || data[i][auxBot] == JUGADORM)
					return false;
				else
					auxBot--;

			for(int k=auxBot+1;k<auxUpp;k++) {
				if(data[i-1][k] != MURO)
					return false;
			}
			return true;
		}

		if(data[i+1][j] == MURO) {
			auxUpp = j;
			while(data[i][auxUpp] != MURO) 
				if(data[i][auxUpp] == META || data[i][auxUpp] == JUGADORM)
					return false;
				else
					auxUpp++;
			auxBot = j;
			while(data[i][auxBot] != MURO) 
				if(data[i][auxBot] == META || data[i][auxBot] == JUGADORM)
					return false;
				else
					auxBot--;

			for(int k=auxBot+1;k<auxUpp;k++) {
				if(data[i+1][k] != MURO)
					return false;
			}
			return true;
		}
		return false;
	}

	private boolean cajasEnBloque(int i, int j) {
		if(data[i+1][j] == CAJA || data[i+1][j] == CAJAM || data[i+1][j] == MURO) {
			if(data[i][j+1] == CAJA || data[i][j+1] == CAJAM || data[i][j+1] == MURO) {
				if(data[i+1][j+1] == CAJA || data[i+1][j+1] == CAJAM || data[i+1][j+1] == MURO) {
					return true;
				}
			}
			if(data[i][j-1] == CAJA || data[i][j-1] == CAJAM || data[i][j-1] == MURO) {
				if(data[i+1][j-1] == CAJA || data[i+1][j-1] == CAJAM || data[i+1][j-1] == MURO) {
					return true;
				}
			}
		}
		if(data[i-1][j] == CAJA || data[i-1][j] == CAJAM || data[i-1][j] == MURO) {
			if(data[i][j+1] == CAJA || data[i][j+1] == CAJAM || data[i][j+1] == MURO) {
				if(data[i-1][j+1] == CAJA || data[i-1][j+1] == CAJAM || data[i-1][j+1] == MURO) {
					return true;
				}
			}
			if(data[i][j-1] == CAJA || data[i][j-1] == CAJAM || data[i][j-1] == MURO) {
				if(data[i-1][j-1] == CAJA || data[i-1][j-1] == CAJAM || data[i-1][j-1] == MURO) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean cajas2ContraPared(int i, int j) {
		if(data[i+1][j] == CAJA || data[i+1][j] == CAJAM) {
			if(data[i][j-1] == MURO && data[i+1][j-1] == MURO) {
				return true;
			}
			if(data[i][j+1] == MURO && data[i+1][j+1] == MURO) {
				return true;
			}
		}
		if(data[i-1][j] == CAJA || data[i-1][j] == CAJAM) {
			if(data[i][j-1] == MURO && data[i-1][j-1] == MURO) {
				return true;
			}
			if(data[i][j+1] == MURO && data[i-1][j+1] == MURO) {
				return true;
			}
		}
		if(data[i][j+1] == CAJA || data[i][j+1] == CAJAM) {
			if(data[i-1][j] == MURO && data[i-1][j+1] == MURO) {
				return true;
			}
			if(data[i+1][j] == MURO && data[i+1][j+1] == MURO) {
				return true;
			}
		}
		if(data[i][j-1] == CAJA || data[i][j-1] == CAJAM) {
			if(data[i-1][j] == MURO && data[i-1][j-1] == MURO) {
				return true;
			}
			if(data[i+1][j] == MURO && data[i+1][j-1] == MURO) {
				return true;
			}
		}
		return false;
	}

	public Tablero empujarCaja(int x, int y, int dir) {
		Tablero next;
		next = (Tablero) this.clone();

		if(next.getData()[charX][charY] == JUGADORM)
			next.getTrueData()[charX][charY] = META;
		else
			next.getTrueData()[charX][charY] = LIBRE;


		if(next.getData()[x][y] == CAJAM)
			next.getTrueData()[x][y] = JUGADORM;
		else
			next.getTrueData()[x][y] = JUGADOR;

		next.setPlayerX(x);
		next.setPlayerY(y);

		switch(dir) {
		case(1):{
			if(next.getData()[x][y-1] == LIBRE)
				next.getTrueData()[x][y-1] = CAJA;
			else
				next.getTrueData()[x][y-1] = CAJAM;
			break;
		}
		case(2):{
			if(next.getData()[x-1][y] == LIBRE)
				next.getTrueData()[x-1][y] = CAJA;
			else
				next.getTrueData()[x-1][y] = CAJAM;
			break;
		}
		case(3):{
			if(next.getData()[x][y+1] == LIBRE)
				next.getTrueData()[x][y+1] = CAJA;
			else
				next.getTrueData()[x][y+1] = CAJAM;
			break;
		}
		case(4):{
			if(next.getData()[x+1][y] == LIBRE)
				next.getTrueData()[x+1][y] = CAJA;
			else
				next.getTrueData()[x+1][y] = CAJAM;
			break;
		}

		}

		next.setValor();
		return next;
	}

	public boolean equals(Tablero o) {
		if(o.getWidth() == this.width && o.getHeight() == this.height) {
			for(int i=0;i<width;i++) {
				for(int j=0;j<height;j++) {
					if(o.getData()[i][j] != JUGADOR) {// || o.getData()[i][j] != JUGADORM) {
						if(this.data[i][j] != JUGADOR) {// || this.data[i][j] != JUGADORM) {
							if(o.getData()[i][j] != this.data[i][j])
								return false;
						}
					}
				}
			}
			if(o.puedeLlegar(o.getPlayerX(), o.getPlayerY(), charX, charY)){
				return true;
			}
		}
		return false;
	}

	public Tablero clone() {
		Tablero copia = new Tablero(width,height,data,charX,charY);
		return copia;
	}

	public void showTab() {
		for(int i=0;i<width;i++) {
			System.out.println();
			for(int j=0;j<height;j++) {
				System.out.print(data[j][i]);
			}
		}
		System.out.println();
		System.out.println();
	}

	public boolean terminado() {
		for(int i=0;i<width;i++) 
			for(int j=0;j<height;j++) 
				if(data[i][j] == CAJA) 
					return false;
		return true;
	}

	public String toString() {
		String x = "";
		for(int i=0;i<width;i++) {
			for(int j=0;j<height;j++) {
				x = x + data[j][i];
			}
			x = x + "\n";
		}

		return x;
	}

	public static int direccionMovimiento(Tablero a, Tablero b) {

		for(int i=0;i<a.getWidth();i++) {
			for(int j=0;j<a.getHeight();j++) {
				if( (a.getData()[i][j] == CAJA || a.getData()[i][j] == CAJAM) &&
						(b.getData()[i][j] != CAJA && b.getData()[i][j] != CAJAM)) {

					if((b.getData()[i-1][j] == CAJA || b.getData()[i-1][j] == CAJAM)
							&&(a.getData()[i-1][j] != CAJA && a.getData()[i-1][j] != CAJAM)) {
						return 4;
					}
					else if((b.getData()[i][j-1] == CAJA || b.getData()[i][j-1] == CAJAM)
							&&(a.getData()[i][j-1] != CAJA && a.getData()[i][j-1] != CAJAM)) {
						return 1;
					}
					else if((b.getData()[i+1][j] == CAJA || b.getData()[i+1][j] == CAJAM)
							&&(a.getData()[i+1][j] != CAJA && a.getData()[i+1][j] != CAJAM)) {
						return 2;
					}
					else if((b.getData()[i][j+1] == CAJA || b.getData()[i][j+1] == CAJAM)
							&&(a.getData()[i][j+1] != CAJA && a.getData()[i][j+1] != CAJAM)) {
						return 3;
					}
				}
			}
		}
		return 0;
	}
}
