import java.util.ArrayList;

public class Nodo {
	private Tablero dato;
	private ArrayList<Nodo> hijos;

	public Nodo(Tablero x) {
		this.dato = x;
		this.hijos = new ArrayList<>();
	}

	public Tablero getDato() {
		return this.dato;
	}

	public boolean search(Tablero x) {
		if(this.dato.equals(x)) 
			return true;
		else {
			for(int i=0;i<this.hijos.size();i++) {
				if(hijos.get(i).search(x))
					return true;
			}
			return false;
		}
	}

	public boolean add(Tablero newAdd, Tablero padre) {
		if(this.dato.equals(padre)) { 
			Nodo nuevo = new Nodo (newAdd);
			hijos.add(nuevo);
			return true;
		}
		else {
			for(int i=0;i<this.hijos.size();i++) {
				if(hijos.get(i).add(newAdd, padre)) {
					return true;
				}
			}
		}
		return false;
	}


	public ArrayList<Tablero> resolver(ArrayList<Tablero> solucion, Tree recuerdos) {
		// TODO
		if(this.dato.terminado()) {
			solucion.add(this.dato);
			return solucion;
		}

		Tablero candidato = null;
		int cont = 0;
		do {
			cont++;
			boolean diferente = true, deadlock = false;
			candidato = generarCamino(this.dato, cont);
			if(candidato != null) {
				if(candidato.deadlocked()) {
					deadlock = true;
				}
				else {
					if(recuerdos.search(candidato) == true) {
						diferente = false;
					}
				}

				if(diferente && !deadlock) {
					Nodo nuevo = new Nodo(candidato);
					this.hijos.add(nuevo);
				}
			}

		}while(candidato != null);

		this.hijos.sort(new TreeCompare());

		for(Nodo hijoActual : hijos) {
			solucion = hijoActual.resolver(solucion, recuerdos);
			if(!solucion.isEmpty()) {
				solucion.add(0, this.dato);
				return solucion;
			}
		}

		return solucion;
	}

	private Tablero generarCamino(Tablero tab, int iter) {
		int contG = 0, contX = 0, contY = 0;
		int dir = 0;
		while(iter > 0) {
			contG++;
			contX = contG%tab.getWidth();
			contY = contG/tab.getHeight();

			if(contY==tab.getHeight()) {
				return null;
			}

			if(tab.getPos(contX,contY) == Tablero.CAJA || tab.getPos(contX,contY) == Tablero.CAJAM) {
				dir++;
				if(dir == 5) {
					dir = 0;
				}
				else if(tab.posibleEmpujarCaja(contX,contY,dir)) {
					iter--;
					contG--;
				}
				else {
					contG--;
				}

			}
		}

		Tablero next = tab.empujarCaja(contX,contY,dir);
		return next;
	}
	
	public String toString() {
		return dato.toString();
	}

}
