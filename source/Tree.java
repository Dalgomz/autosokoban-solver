import java.util.ArrayList;

public class Tree {
	private Nodo raiz;
	
	public Tree(Tablero x) {
		this.raiz = new Nodo(x);
	}
	
	public boolean search(Tablero x) {
		return raiz.search(x);
	}
	
	public boolean add(Tablero newAdd, Tablero padre) {
		return raiz.add(newAdd,padre);
	}
	
	public ArrayList<Tablero> resolver(){
		ArrayList<Tablero> solucion = new ArrayList<>();
		solucion = raiz.resolver(solucion, this);
		return solucion;
	}
}
