import java.util.Comparator;

public class AStarCompare implements Comparator<AStarNodo> {

	@Override
	public int compare(AStarNodo a, AStarNodo b) {
		
		return a.valorH - b.valorH;
	}
}
