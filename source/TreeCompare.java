import java.util.Comparator;

public class TreeCompare implements Comparator<Nodo> {

	@Override
	public int compare(Nodo a, Nodo b) {
		
		return a.getDato().getValor() - b.getDato().getValor() ;
	}
	
}
