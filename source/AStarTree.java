import java.util.ArrayList;
public class AStarTree {

	private AStarNodo raiz;

	public AStarTree(boolean x[][], int posx, int posy,int mx, int my) {
		this.raiz = new AStarNodo(x,posx,posy,mx,my);
	}

	public boolean search(int posx, int posy) {
		return raiz.search(posx, posy);
	}
	
	public ArrayList<Character> resolver(){
		ArrayList<Character> solucion = new ArrayList<>();
		solucion = raiz.resolver(solucion, this);
		return solucion;
	}
	
	
}
